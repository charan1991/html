# HTML basics

___

HTML consists of 3 major sections -
1. HTML
* It is the root element of the HTML page or the document
2. HEAD
* It contains meta information about the document
3. BODY
* It contains visible page content

## DOCTYPE
* It defines document type and helps to display web pages correctly.
* All HTML documents must start with document type declaration **<!DOCTYPE>**

#### SEMANTICS
* It basically describes clear meaning to both developer and browser.
* For example **div** and **span** elements does not tell anything about their content.

Whereas elements such as **form**, **article**, **table** clearly define their content.

* It allows data to be shared and reused across applications, communities and enterprises.

#### ELEMENTS AND ATTRIBUTES

* Elements have start tag and end tag with content between them.
* Elements without content are called empty or void elements like **br** tag.
* Attributes provide additional information about elements and specified in name/value pairs.
* Attribute values can be single or double quoted.

###### BLOCK AND INLINE ELEMENTS
* Every HTML element has default display value, depending on the type of element.
* Display values are block and inline.
* Block level elements always start on a new line and takes up the full width available from left to right.
* Inline level elements does not start on new line and takes as much width as necessary.